(deftheme futura2
  "Created 2018-03-29.")

(custom-theme-set-faces
 'futura2
 '(default ((t (:inherit nil :stipple nil :background "#FAFAFA" :foreground "gray30" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 100 :width normal :foundry "nil" :family "Overpass Mono"))))
 '(cursor ((t (:background "#FF5722"))))
 '(fixed-pitch ((t (:family "Monospace"))))
 '(variable-pitch ((((type w32)) (:foundry "outline" :family "Arial")) (t (:family "Sans Serif"))))
 '(escape-glyph ((((background dark)) (:foreground "cyan")) (((type pc)) (:foreground "magenta")) (t (:foreground "brown"))))
 '(minibuffer-prompt ((t (:foreground "#2196f3"))))
 '(highlight ((t (:inverse-video nil :background "#ECEFF1"))))
 '(region ((t (:background "#90A4AE"))))
 '(shadow ((t (:foreground "#607d8b"))))
 '(secondary-selection ((t (:background "#bf616a"))))
 '(trailing-whitespace ((t (:underline nil :inverse-video t :foreground "#B71C1C"))))
 '(font-lock-builtin-face ((t (:foreground "#B71C1C"))))
 '(font-lock-comment-delimiter-face ((t (:foreground "#607d8b"))))
 '(font-lock-comment-face ((t (:foreground "#607d8b"))))
 '(font-lock-constant-face ((t (:foreground "#558b2f"))))
 '(font-lock-doc-face ((t (:foreground "#673ab7"))))
 '(font-lock-function-name-face ((t (:foreground "#0097A7"))))
 '(font-lock-keyword-face ((t (:foreground "#00796b"))))
 '(font-lock-negation-char-face ((t (:foreground "#2196f3"))))
 '(font-lock-preprocessor-face ((t (:foreground "#FFA000"))))
 '(font-lock-regexp-grouping-backslash ((t (:foreground "#FFA000"))))
 '(font-lock-regexp-grouping-construct ((t (:foreground "#4527A0"))))
 '(font-lock-string-face ((t (:foreground "#689f38"))))
 '(font-lock-type-face ((t (:foreground "#0097A7"))))
 '(font-lock-variable-name-face ((t (:foreground "#EF6C00"))))
 '(font-lock-warning-face ((t (:weight bold :foreground "#B71C1C"))))
 '(button ((t (:inherit (link)))))
 '(link ((t (:underline (:color foreground-color :style line)))))
 '(link-visited ((default (:inherit (link))) (((class color) (background light)) (:foreground "magenta4")) (((class color) (background dark)) (:foreground "violet"))))
 '(fringe ((t (:background "#ECEFF1"))))
 '(header-line ((t (:foreground "#4527A0" :inherit (mode-line)))))
 '(tooltip ((((class color)) (:inherit (variable-pitch) :foreground "black" :background "lightyellow")) (t (:inherit (variable-pitch)))))
 '(mode-line ((t (:box (:line-width 2 :color "#ECEFF1" :style nil) :foreground "#212121" :background "#e0f7fa"))))
 '(mode-line-buffer-id ((t (:weight bold :foreground "#212121"))))
 '(mode-line-emphasis ((t (:slant italic :foreground "#212121"))))
 '(mode-line-highlight ((t (:box nil :foreground "#4527A0"))))
 '(mode-line-inactive ((t (:weight normal :foreground "#a7adba" :background "#ECEFF1" :inherit (mode-line)))))
 '(isearch ((t (:foreground "#FAFAFA" :background "#558b2f"))))
 '(isearch-fail ((t (:inverse-video t :background "#FAFAFA" :inherit (font-lock-warning-face)))))
 '(lazy-highlight ((t (:inverse-video nil :foreground "#FAFAFA" :background "#558b2f"))))
 '(match ((t (:inverse-video nil :foreground "#FAFAFA" :background "#558b2f"))))
 '(next-error ((t (:inherit (region)))))
 '(query-replace ((t (:inherit (isearch)))))
 '(linum ((t (:background "gray98" :foreground "gray85")))))

(provide-theme 'futura2)
