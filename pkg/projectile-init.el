(require 'projectile)

(projectile-global-mode)
(global-set-key (kbd "C-c m") 'projectile-vc)
