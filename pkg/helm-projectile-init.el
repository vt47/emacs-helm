(require 'helm-projectile)
(helm-projectile-on)

(global-set-key (kbd "C-;") 'helm-projectile-find-file)
(global-set-key (kbd "C-c k") 'helm-projectile-ag)
