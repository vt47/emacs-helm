(require 'avy)

(global-set-key (kbd "C-c c") 'avy-goto-char)
(global-set-key (kbd "C-c f") 'avy-goto-char-in-line)
