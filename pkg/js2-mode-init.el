(require 'js2-mode)
(require 'js2-refactor)
(require 'tern)

(add-to-list 'auto-mode-alist '("\\.js[x]\\'" . js2-mode))
(add-hook 'js2-mode-hook #'js2-imenu-extras-mode)
(add-hook 'js2-mode-hook #'js2-refactor-mode)
(add-hook 'js2-mode-hook (lambda ()
                           (tern-mode)
                           (company-mode)))
